import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Grievances from './views/Grievances.vue'

// AUTH
// import Store from '@/store'
// import {
//   AmplifyPlugin
// } from 'aws-amplify-vue';
// import {
//   Auth
// } from 'aws-amplify'

// AUTH
//Vue.use(Router, Auth, AmplifyPlugin);
// BASE
Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [{
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/grievances',
      name: 'Grievances',
      component: Grievances,
      // AUTH
      // meta: {
      //   requiresAuth: true
      // }
    },
    {
      path: '/feats',
      name: 'feats',
      component: () =>
        import( /* webpackChunkName: "about" */ './views/Feats.vue')
    },
    {
      path: '/miracles',
      name: 'miracles',
      component: () =>
        import( /* webpackChunkName: "about" */ './views/Miracles.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () =>
        import( /* webpackChunkName: "about" */ './views/About.vue')
    },

    // AUTH
    // {
    //   path: '/auth',
    //   name: 'auth',
    //   component: () =>
    //     import( /* webpackChunkName: "about" */ './views/Auth.vue')
    // }
  ]
})

// AUTH
// function getUser() {
//   return Auth.currentAuthenticatedUser({
//       bypassCache: false
//     }).then((data) => {
//     if (data && data.signInUserSession && data.signInUserSession.isValid()) {
//       Store.dispatch('setUser', 'valid');
//       //  Auth.currentCredentials()
//       //    .then(credentials => {
//       //      Store.dispatch('setUser', credentials.identityId);
//       //    });
//       //  return data;
//     }
//   }).catch((e) => {
//     console.log(e);
//     Store.dispatch('setUser', null);
//     // return null;
//   });
// }
//
// router.beforeResolve(async (to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     await getUser();
//     if (!Store.state.user) {
//       return next({
//         path: '/auth',
//         query: {
//           redirect: to.fullPath,
//         }
//       });
//     }
//     return next();
//   }
//   return next();
// })
// END AUTH

router.push({
  path: '/'
})

export default router
